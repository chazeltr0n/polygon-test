def find_shortest_intersecting_point(event_point, poly_point1, poly_point2)
  a = poly_point1[1] - poly_point2[1]
  b = poly_point2[0] - poly_point1[0]
  c = poly_point1[0] * poly_point2[1] - poly_point1[1] * poly_point2[0]

  t = a * event_point[0] + b * event_point[1] + c

  x = event_point[0] - (a * t) / (a**2 + b**2)
  y = event_point[1] - (b * t) / (a**2 + b**2)

  [ x, y ]
end

poly1 = [ 71.123, -40.234 ] # polygon point 1
event = [ 72.333, -39.876 ] # event radius center
poly2 = [ 72.888, -41.021 ] # polygon point 2

# Equation:
# a*x + b*y + c = 0

p find_shortest_intersecting_point(event, poly1, poly2)
