A quick tool to test subscriptions vs. events.

Just modify the code in the `run_tests.rb` file to include the event lat/lon/radius and the subscription WKT string.
