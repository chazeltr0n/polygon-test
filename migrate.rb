require 'sqlite3'
require 'active_record'

ActiveRecord::Base.establish_connection(
  :adapter   => 'sqlite3',
  :database  => './polygon.db'
)

begin
  ActiveRecord::Migration.class_eval do
    create_table :events do |t|
      t.string  :name
      t.float   :latitude
      t.float   :longitude
      t.float   :radius
    end
  end
rescue => e
  STDERR.puts "AWW SHIT no events"
end

begin
  ActiveRecord::Migration.class_eval do
    create_table :subscriptions do |t|
      t.string  :name
      t.string  :wkt
    end
  end
rescue => e
  STDERR.puts "AWW SHIT no subscriptions"
end
