require 'rubygems'
require 'active_record'
require 'geocoder'

class Polygon
  ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'store.db')

  class Event < ActiveRecord::Base

    def hit?(subscription)
      any_within_circle?(subscription.polygon) || perpendicular_point_is_within_circle?(subscription.polygon)
    end

    # once you have one, you can break; no need to loop through all
    def any_within_circle?(polygon)
      boolean = false

      polygon.each do |points|
        boolean = Geocoder::Calculations.distance_between([ points[0], points[1] ], [ self.longitude, self.latitude ]) <= self.radius
        break if boolean
      end

      boolean
    end

    # once you have one, you can break; no need to loop through all
    def perpendicular_point_is_within_circle?(polygon)
      i = 0

      polygon.any? do |points|
        if polygon[i+1].nil?
          # for that one time we need to check the line formed by the last array
          # element and the first array element.
          shortest_point = perpendicular_point_for_line_segment(self.longitude,
                                                                self.latitude,
                                                                polygon[i][0],
                                                                polygon[i][1],
                                                                polygon[0][0],
                                                                polygon[0][1])
        else
          shortest_point = perpendicular_point_for_line_segment(self.longitude,
                                                                self.latitude,
                                                                polygon[i][0],
                                                                polygon[i][1],
                                                                polygon[i+1][0],
                                                                polygon[i+1][1])
        end

        distance = Geocoder::Calculations.distance_between(shortest_point, [ self.longitude, self.latitude ])

        i += 1

        distance <= self.radius
      end
    end

    def perpendicular_point_for_line_segment(event_x, event_y, line_segment_x1, line_segment_y1, line_segment_x2, line_segment_y2)
      a = line_segment_y1 - line_segment_y2
      b = line_segment_x2 - line_segment_x1
      c = line_segment_x1 * line_segment_y2 - line_segment_y1 * line_segment_x2

      t = a * event_x + b * event_y + c
      x = event_x - (a * t) / (a**2 + b**2)
      y = event_y - (b * t) / (a**2 + b**2)
      return [x, y]

      # dx = line_segment_x2 - line_segment_x1
      # dy = line_segment_y2 - line_segment_y1
      # d  = dx*dx + dy*dy
      # c1 = line_segment_x1*dy - line_segment_y1*dx
      # c2 = event_x*dx + event_y*dy
      # x  = (dy*c1 + dx*c2)/d
      # y  = (-dx*c1 + dy*c2)/d
      # return [x, y]
    end
  end


  class Subscription < ActiveRecord::Base

    def polygon
      @memoized_polygon ||= wkt.split("(")[2..wkt.size][0].split(',').map {|y| y.split(" ").map(&:to_f) }
    end

    def google_maps_polypoints
      buf = "["

      polygon.each_with_index do |points, index|
        buf += "          new google.maps.LatLng(#{points[1]}, #{points[0]})"

        unless index == (polygon.size - 1)
          buf += ","
        end
      end

      buf += "]"
    end

  end

end
